<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleSaveData"/>

<form:form method="POST" modelAttribute="nontravelclaim" action="<%=submitFormURL.toString() %>">
<div class="form-group">
				     <label class="control-label col-sm-4" for="type of claim">
				     <b>
					    Type of Claim : Cab
					 </b>
					 </label>
			
				</div>
			
			
				<br>
				<div class="form-group">
				<label class="control-label col-sm-4" for="completed date">Date for Claim :</label>
				<div class="col-sm-4"><form:input type="date" class="form-control"  path="dateForClaim"></form:input>
				</div>
				</div>
				<br>
				
				<div class="form-group">
				<label class="control-label col-sm-4" for="certification fees">Amount :</label>
				<div class="col-sm-4">
				<form:input path="claimAmount" class="form-control" placeholder="" ></form:input>
				</div>
				<label>INR</label>
				</div>
				<br>
				
				<div class="col-sm-8  pull-right">
				<button class="btn btn-info btn-color" type="submit">Submit</button>
				</div>
				
				
</form:form>