package com.hrms.service;

import java.util.List;



import com.hrms.entity.EducationDetails;
import com.hrms.entity.Employee;

public interface EducationDetailsService {

	
	
	List<EducationDetails> getEducationDetail();
	void saveEducationDetails(EducationDetails educationdetails);
//	boolean updateEducationDetails(EducationDetails educationdetails);
	void deleteEducationDetails(EducationDetails educationdetails);
	EducationDetails getEducationDetailsByEduId(Long eduId);
	EducationDetails getEducationDetailsByEmpId(Long empId);
	boolean updateEducationDetails(EducationDetails educationdetails, long eduId);


}