package com.hrms.service;

import java.util.List;
import com.hrms.entity.Employee;
import com.hrms.entity.SkillDetails;

public interface SkillDetailsService {


	
	List<SkillDetails> getSkillDetails();
	void saveSkillDetails(SkillDetails employeeproject);
	boolean updateSkillDetail(SkillDetails EmployeeProject);
	void deleteSkillDetail(SkillDetails EmployeeProject);
	SkillDetails getSkillById(long id);
	List<SkillDetails> getSkillByEmpId(long empId);

}
