package com.hrms.service;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Address;
import com.hrms.entity.NonTravelClaim;



public interface NonTravelClaimService {

	
	
	void saveNonTravelClaim(NonTravelClaim nontravelclaim);
	void updateNonTravelClaim(NonTravelClaim nontravelclaim);
	
	NonTravelClaim getNonTravelClaim(NonTravelClaim nontravelclaim);
	
	
	
}
