
package com.hrms.service.Impl;

import java.util.List;

import com.hrms.dao.hibernate.EmployeeProjectDao;
import com.hrms.entity.Employee;
import com.hrms.entity.EmployeeProject;
import com.hrms.service.EmployeeProjectService;

public class EmployeeProjectServiceImpl implements EmployeeProjectService{

	
	
	private EmployeeProjectDao employeeProjectDao;
	
	@Override
	public List<EmployeeProject> getEmployeeProject() {
		// TODO Auto-generated method stub
		return employeeProjectDao.getEmployeeProject();
	}

	@Override
	public void saveEmployeeProject(EmployeeProject employeeproject) {
		// TODO Auto-generated method stub
		employeeProjectDao.saveEmployeeProject(employeeproject);
	}

	@Override
	public boolean updateEmployeeProject(EmployeeProject EmployeeProject) {
		// TODO Auto-generated method stub
		return employeeProjectDao.updateEmployeeProject(EmployeeProject);
	}

	@Override
	public void deleteEmployeeProject(EmployeeProject EmployeeProject) {
		// TODO Auto-generated method stub
		employeeProjectDao.deleteEmployeeProject(EmployeeProject);
	}

	@Override
	public EmployeeProject getProjectById(Long id) {
		// TODO Auto-generated method stub
		return employeeProjectDao.getProjectById(id);
	}

	@Override
	public List<EmployeeProject> getProjectByEmpId(long empId) {
		// TODO Auto-generated method stub
		return employeeProjectDao.getProjectByEmpId(empId);
	}

	public EmployeeProjectDao getEmployeeProjectDao() {
		return employeeProjectDao;
	}

	public void setEmployeeProjectDao(EmployeeProjectDao employeeProjectDao) {
		this.employeeProjectDao = employeeProjectDao;
	}

}
