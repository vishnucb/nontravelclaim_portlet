package com.hrms.service.Impl;

import java.util.List;

import com.hrms.dao.hibernate.SkillDetailsDao;
import com.hrms.entity.Employee;
import com.hrms.entity.SkillDetails;
import com.hrms.service.SkillDetailsService;

public class SkillDetailsServiceImpl implements SkillDetailsService{
	
	
	
	private SkillDetailsDao skillDetailsDao;
	
	
	@Override
	public List<SkillDetails> getSkillDetails() {
		// TODO Auto-generated method stub
		return skillDetailsDao.getAllSkillDetails();
	}

	@Override
	public void saveSkillDetails(SkillDetails employeeproject) {
		// TODO Auto-generated method stub
		skillDetailsDao.saveSkillDetails(employeeproject);
	}

	@Override
	public boolean updateSkillDetail(SkillDetails EmployeeProject) {
		// TODO Auto-generated method stub
		return skillDetailsDao.updateSkillDetail(EmployeeProject);
	}

	@Override
	public void deleteSkillDetail(SkillDetails EmployeeProject) {
		// TODO Auto-generated method stub
		skillDetailsDao.deleteSkillDetail(EmployeeProject);
	}

	@Override
	public List<SkillDetails> getSkillByEmpId(long empId) {
		// TODO Auto-generated method stub
		return skillDetailsDao.getSkillByEmpId(empId);
	}

	public SkillDetailsDao getSkillDetailsDao() {
		return skillDetailsDao;
	}

	public void setSkillDetailsDao(SkillDetailsDao skillDetailsDao) {
		this.skillDetailsDao = skillDetailsDao;
	}

	@Override
	public SkillDetails getSkillById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
}
