package com.hrms.service.Impl;

import java.util.List;
import com.hrms.dao.hibernate.AddressDao;
import com.hrms.entity.Address;
import com.hrms.service.AddressService;

public class AddressSeviceImpl implements AddressService{

	
	private AddressDao  addressDao;
	@Override
	public List<Address> getAddressDetails() {
		// TODO Auto-generated method stub
		return addressDao.getAddressList();
	}

	@Override
	public void saveAddressDetails(Address addressDetails) {
		// TODO Auto-generated method stub
		addressDao.saveAddress(addressDetails);
	}

	@Override
	public boolean updateAddressDetails(Address addressDetails) {
		// TODO Auto-generated method stub
		return addressDao.updateAddress(addressDetails);
	}

	@Override
	public void deleteAddressDetails(Address addressDetails) {
		// TODO Auto-generated method stub
		addressDao.deleteAddress(addressDetails);
	}

	@Override
	public Address getAddressById(Long id) {
		// TODO Auto-generated method stub
		return addressDao.getAddressByAddressType(id);
	}

/*	@Override
	public Address getAddressByEmpId(String name) {
		// TODO Auto-generated method stub
		return addressDao.;
	}*/

	public AddressDao getAddressDao() {
		return addressDao;
	}

	public void setAddressDao(AddressDao addressDao) {
		this.addressDao = addressDao;
	}

	
	
	
	
}
