package com.hrms.service.Impl;

import java.util.List;

import com.hrms.dao.hibernate.CertificateDetailsDao;
import com.hrms.entity.CertificationDetails;
import com.hrms.service.CertificateDetailsService;

public class CertificateDetailsServiceImpl implements CertificateDetailsService{
	
		private CertificateDetailsDao certificateDao;
	
		
		public CertificateDetailsDao getCertificateDao() {
			return certificateDao;
		}
		
		public void setCertificateDao(CertificateDetailsDao certificateDao) {
			this.certificateDao = certificateDao;
		}

		@Override
		public List<CertificationDetails> getCertificationList() {
			// TODO Auto-generated method stub
			return certificateDao.getCertificationList();
		}

		@Override
		public void saveCertification(CertificationDetails certification) {
			// TODO Auto-generated method stub
			certificateDao.saveCertification(certification);
		}

		@Override
		public boolean updateCertification(CertificationDetails certification) {
			// TODO Auto-generated method stub
			return certificateDao.updateCertification(certification);
		}

		@Override
		public void deleteCertification(CertificationDetails certification) {
			// TODO Auto-generated method stub
			certificateDao.deleteCertification(certification);
		}

		@Override
		public CertificationDetails getAddressById(Long id) {
			// TODO Auto-generated method stub
			return certificateDao.getAddressById(id);
		}

		@Override
		public CertificationDetails getAddressByCertificationType(
				Long certificationTypeId) {
			// TODO Auto-generated method stub
			return certificateDao.getAddressByCertificationType(certificationTypeId);
		}







}
