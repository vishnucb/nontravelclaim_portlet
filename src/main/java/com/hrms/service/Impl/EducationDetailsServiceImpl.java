package com.hrms.service.Impl;

import java.util.List;

import com.hrms.dao.hibernate.EducationDetailsDao;
import com.hrms.entity.EducationDetails;
import com.hrms.entity.Employee;
import com.hrms.service.EducationDetailsService;

public class EducationDetailsServiceImpl implements EducationDetailsService{

	private EducationDetailsDao educationDetailsDao;
	
	@Override
	public List<EducationDetails> getEducationDetail() {
		// TODO Auto-generated method stub
		return educationDetailsDao.getEducationDetail();
	}

	@Override
	public void saveEducationDetails(EducationDetails educationdetails) {
		// TODO Auto-generated method stub
		educationDetailsDao.saveEducationDetails(educationdetails);
	}

	@Override
	public boolean updateEducationDetails(EducationDetails educationdetails, long eduId) {
		// TODO Auto-generated method stub
		return educationDetailsDao.updateEducationDetails(educationdetails, eduId);
	}

	@Override
	public void deleteEducationDetails(EducationDetails educationdetails) {
		// TODO Auto-generated method stub
		educationDetailsDao.deleteEducationDetails(educationdetails);
	}

	@Override
	public EducationDetails getEducationDetailsByEduId(Long eduId) {
		// TODO Auto-generated method stub
		return educationDetailsDao.getEducationDetailsByEduId(eduId);
	}

	@Override
	public EducationDetails getEducationDetailsByEmpId(Long empId) {
		// TODO Auto-generated method stub
		return educationDetailsDao.getEducationDetailsByEduId(empId);
	}

	public EducationDetailsDao getEducationDetailsDao() {
		return educationDetailsDao;
	}

	public void setEducationDetailsDao(EducationDetailsDao educationDetailsDao) {
		this.educationDetailsDao = educationDetailsDao;
	}
	
	
	public static void main(String[] args)
	{
		
	}
}

