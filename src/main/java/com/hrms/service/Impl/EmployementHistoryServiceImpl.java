package com.hrms.service.Impl;

import java.util.List;

import com.hrms.dao.hibernate.EmployementHistoryDao;
import com.hrms.entity.EmployeeProject;
import com.hrms.entity.EmployementHistory;
import com.hrms.service.EmployementHistoryService;

public class EmployementHistoryServiceImpl implements EmployementHistoryService{

	
	private EmployementHistoryDao employeeHistoryDao;
	
	@Override
	public List<EmployementHistory> getEmployementHistory(long empId) {
		// TODO Auto-generated method stub
		return employeeHistoryDao.getEmployementHistory(empId);
	}

	@Override
	public void saveEmployementHistory(EmployementHistory employementhistory) {
		// TODO Auto-generated method stub
		employeeHistoryDao.saveEmployementHistory(employementhistory);

	}

	@Override
	public boolean updateEmployementHistory(EmployementHistory employementhistory) {
		// TODO Auto-generated method stub
		return employeeHistoryDao.updateEmployementHistory(employementhistory);
	}

	@Override
	public void deleteEmployementHistory(EmployementHistory employementhistory) {
		// TODO Auto-generated method stub
		employeeHistoryDao.deleteEmployementHistory(employementhistory);
	}

	

	
	public EmployementHistoryDao getEmployeeHistoryDao() {
		return employeeHistoryDao;
	}

	public void setEmployeeHistoryDao(EmployementHistoryDao employeeHistoryDao) {
		this.employeeHistoryDao = employeeHistoryDao;
	}

	
}
