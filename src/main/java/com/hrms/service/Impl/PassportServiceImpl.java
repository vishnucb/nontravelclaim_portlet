package com.hrms.service.Impl;

import java.util.List;

import com.hrms.dao.hibernate.PassportDetailsDao;
import com.hrms.entity.Employee;
import com.hrms.entity.PassportDetails;
import com.hrms.service.PasssportService;

public class PassportServiceImpl implements PasssportService{

	private PassportDetailsDao passportDao;
	
	@Override
	public List<PassportDetails> getPassportDetails() {
		// TODO Auto-generated method stub
		return passportDao.getPassportDetails();
	}

	@Override
	public void savePassportDetails(PassportDetails passportDetails) {
		// TODO Auto-generated method stub
		passportDao.savePassportDetails(passportDetails);
	}

	@Override
	public boolean updatePassportDetails(PassportDetails passportDetails) {
		// TODO Auto-generated method stub
		return passportDao.updatePassportDetails(passportDetails);
	}

	@Override
	public void deletePassportDetails(PassportDetails passportDetails) {
		// TODO Auto-generated method stub
		passportDao.deletePassportDetails(passportDetails);
	}

	@Override
	public PassportDetails getPassportById(String id) {
		// TODO Auto-generated method stub
		return passportDao.getPassportByPassportNumber(id);
	}

	@Override
	public PassportDetails getPassportByEmpId(long empId) {
		// TODO Auto-generated method stub
		return passportDao.getPassportByEmpId(empId);
	}

	public PassportDetailsDao getPassportDao() {
		return passportDao;
	}

	public void setPassportDao(PassportDetailsDao passportDao) {
		this.passportDao = passportDao;
	}

	
	
	
}
