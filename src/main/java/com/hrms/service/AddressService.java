package com.hrms.service;

import java.util.List;
import com.hrms.entity.Address;



public interface AddressService {

	
	List<Address> getAddressDetails();
	void saveAddressDetails(Address addressDetails) ;
	boolean updateAddressDetails(Address addressDetails);
	void deleteAddressDetails(Address addressDetails);
	Address getAddressById(Long id);
	//Address getAddressByEmpId(String name);
	
	
	
}
