package com.hrms.service;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.CertificationDetails;

public interface CertificateDetailsService {

	
	List<CertificationDetails> getCertificationList();
	void saveCertification(CertificationDetails certification);
	boolean updateCertification(CertificationDetails certification);
	void deleteCertification(CertificationDetails certification);
	CertificationDetails getAddressById(Long id);
	CertificationDetails getAddressByCertificationType(Long certificationTypeId);
	
	
}
