package com.hrms.service;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Employee;
import com.hrms.entity.PassportDetails;

public interface PasssportService {

	
	List<PassportDetails> getPassportDetails();
	void savePassportDetails(PassportDetails passportDetails);
	boolean updatePassportDetails(PassportDetails passportDetails);
	void deletePassportDetails(PassportDetails passportDetails) ;
	PassportDetails getPassportById(String id);
	PassportDetails getPassportByEmpId(long empId);
	
}
