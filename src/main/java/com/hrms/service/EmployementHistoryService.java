package com.hrms.service;

import java.util.List;
import com.hrms.entity.EmployeeProject;
import com.hrms.entity.EmployementHistory;


public interface EmployementHistoryService {

	
	List<EmployementHistory> getEmployementHistory(long empId);
	void saveEmployementHistory(EmployementHistory employementhistory);
	boolean updateEmployementHistory(EmployementHistory employementhistory);
	void deleteEmployementHistory(EmployementHistory employementhistory);
}
