
package com.hrms.service;

import java.util.List;

import com.hrms.entity.Employee;
import com.hrms.entity.EmployeeProject;


public interface EmployeeProjectService {

	
	List<EmployeeProject> getEmployeeProject();
	void saveEmployeeProject(EmployeeProject employeeproject);
	boolean updateEmployeeProject(EmployeeProject EmployeeProject);
	void deleteEmployeeProject(EmployeeProject EmployeeProject);
	EmployeeProject getProjectById(Long id);
	List<EmployeeProject> getProjectByEmpId(long empId);
	

}
