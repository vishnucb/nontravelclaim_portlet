/*
 * Decompiled with CFR 0_115.
 */
package com.hrms.model;

import java.io.Serializable;
import java.util.Date;

public class NonTravelClaim implements Serializable {
    private int transactionID;
    private String typeOfClaim;
    private Date dateForClaim;
    private double claimAmount;
    private String certificationType;
    private String certificationName;
    private String description;

    public String getTypeOfClaim() {
        return this.typeOfClaim;
    }

    public Date getDateForClaim() {
        return this.dateForClaim;
    }

    public double getClaimAmount() {
        return this.claimAmount;
    }

    public String getCertificationType() {
        return this.certificationType;
    }

    public String getCertificationName() {
        return this.certificationName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setTypeOfClaim(String typeOfClaim) {
        this.typeOfClaim = typeOfClaim;
    }

    public void setDateForClaim(Date dateForClaim) {
        this.dateForClaim = dateForClaim;
    }

    public void setClaimAmount(double claimAmount) {
        this.claimAmount = claimAmount;
    }

    public void setCertificationType(String certificationType) {
        this.certificationType = certificationType;
    }

    public void setCertificationName(String certificationName) {
        this.certificationName = certificationName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = 31 * result + (this.certificationName == null ? 0 : this.certificationName.hashCode());
        result = 31 * result + (this.certificationType == null ? 0 : this.certificationType.hashCode());
        long temp = Double.doubleToLongBits(this.claimAmount);
        result = 31 * result + (int)(temp ^ temp >>> 32);
        result = 31 * result + (this.dateForClaim == null ? 0 : this.dateForClaim.hashCode());
        result = 31 * result + (this.description == null ? 0 : this.description.hashCode());
        result = 31 * result + (this.typeOfClaim == null ? 0 : this.typeOfClaim.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        NonTravelClaim other = (NonTravelClaim)obj;
        if (this.certificationName == null ? other.certificationName != null : !this.certificationName.equals(other.certificationName)) {
            return false;
        }
        if (this.certificationType == null ? other.certificationType != null : !this.certificationType.equals(other.certificationType)) {
            return false;
        }
        if (Double.doubleToLongBits(this.claimAmount) != Double.doubleToLongBits(other.claimAmount)) {
            return false;
        }
        if (this.dateForClaim == null ? other.dateForClaim != null : !this.dateForClaim.equals(other.dateForClaim)) {
            return false;
        }
        if (this.description == null ? other.description != null : !this.description.equals(other.description)) {
            return false;
        }
        if (this.typeOfClaim == null ? other.typeOfClaim != null : !this.typeOfClaim.equals(other.typeOfClaim)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "NonTravelClaim [typeOfClaim=" + this.typeOfClaim + ", dateForClaim=" + this.dateForClaim + ", claimAmount=" + this.claimAmount + ", certificationType=" + this.certificationType + ", certificationName=" + this.certificationName + ", description=" + this.description + "]";
    }

    public int getTransactionID() {
        return this.transactionID;
    }

    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }
}
