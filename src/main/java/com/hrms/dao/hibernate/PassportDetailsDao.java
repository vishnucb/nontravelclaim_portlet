package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Employee;
import com.hrms.entity.PassportDetails;

public interface PassportDetailsDao {

	List<PassportDetails> getPassportDetails()throws HibernateException;

	boolean savePassportDetails(PassportDetails passportDetails) throws HibernateException;
	boolean updatePassportDetails(PassportDetails passportDetails)throws HibernateException;
	boolean deletePassportDetails(PassportDetails passportDetails) throws HibernateException;
	PassportDetails getPassportByPassportNumber(String id)throws HibernateException;

	PassportDetails getPassportByEmpId(long empId);
	
	
	
}
