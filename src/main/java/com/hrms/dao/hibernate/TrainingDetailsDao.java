package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;


import com.hrms.entity.TrainingDetails;

public interface TrainingDetailsDao {

	List<TrainingDetails> getTrainingDetails()throws HibernateException;
	boolean saveTrainingDetails(TrainingDetails trainingdetails) throws HibernateException;
	boolean updateTrainingDetails(TrainingDetails trainingdetails)throws HibernateException;
	boolean deleteTrainingDetails(TrainingDetails trainingdetails) throws HibernateException;
	TrainingDetails getTrainingDetailsByTrainingId(Long id)throws HibernateException;
	
}
