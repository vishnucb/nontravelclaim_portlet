package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;


import com.hrms.entity.EmployeeProject;
import com.hrms.entity.EmployementHistory;


public interface EmployementHistoryDao {
	

	List<EmployementHistory> getEmployementHistory(long empId)throws HibernateException;
	boolean saveEmployementHistory(EmployementHistory employementhistory) throws HibernateException;
	boolean updateEmployementHistory(EmployementHistory employementhistory)throws HibernateException;
	boolean deleteEmployementHistory(EmployementHistory employementhistory) throws HibernateException;
	
}
