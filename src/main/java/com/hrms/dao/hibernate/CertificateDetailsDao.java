package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Address;
import com.hrms.entity.CertificationDetails;

public interface CertificateDetailsDao {

	List<CertificationDetails> getCertificationList() throws HibernateException;
	boolean saveCertification(CertificationDetails certification)throws HibernateException;
	boolean updateCertification(CertificationDetails certification)throws HibernateException;
	boolean deleteCertification(CertificationDetails certification)throws HibernateException;
	CertificationDetails getAddressById(Long id)throws HibernateException;
	CertificationDetails getAddressByCertificationType(Long certificationTypeId)throws HibernateException;
}
