
package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Employee;
import com.hrms.entity.EmployeeProject;


public interface EmployeeProjectDao {
	

	List<EmployeeProject> getEmployeeProject()throws HibernateException;
	boolean saveEmployeeProject(EmployeeProject employeeproject) throws HibernateException;
	boolean updateEmployeeProject(EmployeeProject EmployeeProject)throws HibernateException;
	boolean deleteEmployeeProject(EmployeeProject EmployeeProject) throws HibernateException;
	EmployeeProject getProjectById(long projId)throws HibernateException;
	List<EmployeeProject> getProjectByEmpId(long empId)throws HibernateException;
	

}
