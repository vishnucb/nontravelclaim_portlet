package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Address;

// Address DAO class for getting addresss123
public interface AddressDao {
	
	List<Address> getAddressList() throws HibernateException;
	boolean saveAddress(Address address)throws HibernateException;
	boolean updateAddress(Address address)throws HibernateException;
	boolean deleteAddress(Address address)throws HibernateException;
	Address getAddressById(Long id)throws HibernateException;
	Address getAddressByAddressType(Long addressTypeId)throws HibernateException;
	

}
