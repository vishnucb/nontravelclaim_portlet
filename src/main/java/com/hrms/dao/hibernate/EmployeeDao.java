package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Employee;

public interface EmployeeDao {

	

	List<Employee> getEmployeeList() throws HibernateException;
	boolean saveEmployee(Employee employee)throws HibernateException;
	boolean updateEmployee(Employee employee)throws HibernateException;
	Employee getLogin(String userName,String password)throws HibernateException;
	boolean deleteEmployee(Employee employee)throws HibernateException;
	Employee getEmployeeById(long id)throws HibernateException;
	List<Employee> getEmployeeByName(String name)throws HibernateException;
	List<Employee> getEmployeeByEmailId(String emailId)throws HibernateException;
	public String getEmployeePassword(long empId) throws HibernateException;
	
	
	
	
	
	
}
