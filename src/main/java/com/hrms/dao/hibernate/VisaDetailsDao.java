package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Employee;
import com.hrms.entity.VisaDetails;

public interface VisaDetailsDao {
	
	
	List<VisaDetails> getVisaDetails()throws HibernateException;
	boolean saveVisaDetails(VisaDetails visadetails) throws HibernateException;
	boolean updateVisaDetails(VisaDetails visadetails)throws HibernateException;
	boolean deleteVisaDetails(VisaDetails visadetails) throws HibernateException;
	Employee getEmployeeVisaDetailsByVisaId(Long VisaId)throws HibernateException;

}
