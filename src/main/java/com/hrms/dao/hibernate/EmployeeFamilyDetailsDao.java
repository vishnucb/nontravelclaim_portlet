package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;


import com.hrms.entity.EmployeeFamilyDetails;

public interface EmployeeFamilyDetailsDao {
	
	
	List<EmployeeFamilyDetails> getEmployeeFamilyDetails()throws HibernateException;
	boolean saveEmployeeFamilyDetails(EmployeeFamilyDetails employeefamilydetails) throws HibernateException;
	boolean updateEmployeeFamilyDetails(EmployeeFamilyDetails employeefamilydetails)throws HibernateException;
	boolean deleteEmployeeFamilyDetails(EmployeeFamilyDetails employeefamilydetails) throws HibernateException;
	List<EmployeeFamilyDetails> getEmployeeFamilyDetailsByEmailId(String emailid)throws HibernateException;
	EmployeeFamilyDetails getEmployeeFamilyDetailsByEmpId(Long empId)throws HibernateException;

}
