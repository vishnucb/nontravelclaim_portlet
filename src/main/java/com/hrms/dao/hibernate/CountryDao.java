package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Country;



public interface CountryDao {
	
	List<Country> getCountry()throws HibernateException;
	boolean saveCountry(Country country) throws HibernateException;
	boolean updateCountry(Country country)throws HibernateException;
	boolean deleteCountry(Country country) throws HibernateException;
	List<Country> getCountryByCountryName(String countryname)throws HibernateException;

}
