package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.EducationDetails;
import com.hrms.entity.Employee;

public interface EducationDetailsDao {
	List<EducationDetails> getEducationDetail()throws HibernateException;
	boolean saveEducationDetails(EducationDetails educationdetails) throws HibernateException;
	boolean deleteEducationDetails(EducationDetails educationdetails) throws HibernateException;
	EducationDetails getEducationDetailsByEduId(Long eduId)throws HibernateException;
//	EducationDetails getEducationDetailsByEmpId(Long empId)throws HibernateException;
	boolean updateEducationDetails(EducationDetails educationdetails, long eduId) throws HibernateException;
	EducationDetails getEducationDetailsByEmpId(Long empId) throws HibernateException;

}
