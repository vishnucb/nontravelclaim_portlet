package com.hrms.dao.hibernate;

import org.hibernate.HibernateException;

import com.hrms.entity.NonTravelClaim;


public interface NonTravelClaimDao {
	
	
	boolean saveNonTravelClaim(NonTravelClaim nontravelclaim)throws HibernateException;
	boolean updateNonTravelClaim(NonTravelClaim nontravelclaim)throws HibernateException;
	
	NonTravelClaim getNonTravelClaim(NonTravelClaim nontravelclaim)throws HibernateException;
	
}
