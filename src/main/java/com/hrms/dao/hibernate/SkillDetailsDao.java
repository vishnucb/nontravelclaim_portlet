package com.hrms.dao.hibernate;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Employee;
import com.hrms.entity.SkillDetails;

public interface SkillDetailsDao {
	
	List<SkillDetails> getAllSkillDetails()throws HibernateException;
	boolean saveSkillDetails(SkillDetails employeeproject) throws HibernateException;
	boolean updateSkillDetail(SkillDetails EmployeeProject)throws HibernateException;
	boolean deleteSkillDetail(SkillDetails EmployeeProject) throws HibernateException;
	List<SkillDetails> getSkillByEmpId(long id)throws HibernateException;
	SkillDetails getSkillBySkillId(long skillId)throws HibernateException;

}
