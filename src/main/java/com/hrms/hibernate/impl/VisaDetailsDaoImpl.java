package com.hrms.hibernate.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.VisaDetailsDao;
import com.hrms.entity.Employee;
import com.hrms.entity.VisaDetails;

public class VisaDetailsDaoImpl implements VisaDetailsDao{

	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();
	
	@Override
	public List<VisaDetails> getVisaDetails() throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<VisaDetails> visadetails= session.createQuery("FROM VisaDetails").list();
		return visadetails;
	}

	@Override
	public boolean saveVisaDetails(VisaDetails visadetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		System.out.println("In save visadetails");
		System.out.println(visadetails);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(visadetails);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	@Override
	public boolean updateVisaDetails(VisaDetails visadetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteVisaDetails(VisaDetails visadetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(visadetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting visadetails, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting visadetails, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

	@Override
	public Employee getEmployeeVisaDetailsByVisaId(Long VisaId)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Employee employee;
		try {
			employee= (Employee) session.get(Employee.class, VisaId);
			System.out.println("THE EMPLOYEE DETAILS ARE:" + employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching visa details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching visa details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return employee;
	}

}
