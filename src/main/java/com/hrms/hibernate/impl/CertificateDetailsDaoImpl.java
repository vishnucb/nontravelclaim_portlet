package com.hrms.hibernate.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.CertificateDetailsDao;
import com.hrms.entity.CertificationDetails;

public class CertificateDetailsDaoImpl implements CertificateDetailsDao{
	
	
	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();


	@Override
	public List<CertificationDetails> getCertificationList()
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<CertificationDetails> certification = session.createQuery("FROM CertificationDetails").list();
		return certification;
	}

	

	@Override
	public boolean updateCertification(CertificationDetails certification)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public CertificationDetails getAddressById(Long id)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		CertificationDetails certification;
		try {
			certification= (CertificationDetails) session.get(CertificationDetails.class, id);
			System.out.println("THE CERTFICATION DETAILS ARE:" + certification);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching certification details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching certification details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return certification;
	}

	@Override
	public CertificationDetails getAddressByCertificationType(
			Long certificationTypeId) throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		CertificationDetails certification;
		try {
			certification= (CertificationDetails) session.get(CertificationDetails.class, certificationTypeId);
			System.out.println("THE CERTIFICATION DETAILS ARE:" + certification);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching certification details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching certification details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return certification;
	}



	@Override
	public boolean saveCertification(CertificationDetails certification)
			throws HibernateException {
		// TODO Auto-generated method stub
		System.out.println("In save certification");
		System.out.println(certification);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(certification);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}



	@Override
	public boolean deleteCertification(CertificationDetails certification)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(certification);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting certification, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting certification, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

}
