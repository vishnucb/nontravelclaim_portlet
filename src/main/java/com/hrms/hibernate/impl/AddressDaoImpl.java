package com.hrms.hibernate.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.AddressDao;
import com.hrms.entity.Address;
import com.hrms.entity.AddressType;

// Address DAO class for getting addresss123
public class AddressDaoImpl implements AddressDao {
	
	
	
	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();

	@Override
	public List<Address> getAddressList() throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<Address> address = session.createQuery("FROM Address").list();
		return address;
	}

	

	

	@Override
	public Address getAddressById(Long id) throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Address address;
		try {
			address= (Address) session.get(Address.class, id);
			System.out.println("THE ADDRESS DETAILS ARE:" + address);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching address details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching address details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return address;
	}

	@Override
	public Address getAddressByAddressType(Long addressTypeId)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Address address;
		try {
			address= (Address) session.get(Address.class, addressTypeId);
			System.out.println("THE ADDRESS DETAILS ARE:" + address);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching address details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching address details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return address;
	}





	@Override
	public boolean saveAddress(Address address) throws HibernateException {
		// TODO Auto-generated method stub
		System.out.println("In save address");
		System.out.println(address);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(address);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
		
	}





	@Override
	public boolean updateAddress(Address address) throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}





	@Override
	public boolean deleteAddress(Address address) throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(address);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting address, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting address, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}
	

}
