
package com.hrms.hibernate.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hrms.dao.hibernate.EmployeeProjectDao;
import com.hrms.entity.Employee;
import com.hrms.entity.EmployeeProject;


public class EmployeeProjectDaoImpl implements EmployeeProjectDao{
	
	@Autowired
	private SessionFactory factory = new Configuration().configure()
			.buildSessionFactory();

	HibernateTemplate template;

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}


	@Override  //tested working
	public List<EmployeeProject> getEmployeeProject() throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<EmployeeProject> list = session.createQuery("FROM EmployeeProject").list();
		return list;
		
	}

	@Override
	public boolean saveEmployeeProject(EmployeeProject employeeproject)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(employeeproject);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
		
	}

	@Override
	public boolean updateEmployeeProject(EmployeeProject employeeProject)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx1 = session.beginTransaction();
		session.update(employeeProject);		
		tx1.commit();
		session.close();
		return true;
	}

	@Override
	public boolean deleteEmployeeProject(EmployeeProject employeeProject)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(employeeProject);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting employeeProject, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting employeeProject, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

	@Override
	public EmployeeProject getProjectById(long id) throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		EmployeeProject employeeProject;
		try {
			employeeProject= (EmployeeProject) session.get(Employee.class, id);
			System.out.println("THE EMPLOYEE DETAILS ARE:" + employeeProject);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employeeProject details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employeeProject details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return employeeProject;
	}

	@Override
	public List<EmployeeProject> getProjectByEmpId(long empId) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	

}
