package com.hrms.hibernate.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hrms.dao.hibernate.EmployeeDao;
//<<<<<<< HEAD
//=======

//>>>>>>> branch 'master' of https://Ashwanipandey@bitbucket.org/tulsirock/hrms_dao.git
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hrms.entity.Employee;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Criteria;


public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	private SessionFactory factory = new Configuration().configure()
			.buildSessionFactory();

	HibernateTemplate template;

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}

	@Override  //SaveEmployee working, tested
	public boolean saveEmployee(Employee employee) {
		System.out.println("In save employee");
		System.out.println(employee);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(employee);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	@Override  //tested working
	public boolean updateEmployee(Employee employee) {
		long empId = employee.getEmpId();
		Session session = factory.openSession();
		Transaction tx1 = session.beginTransaction();

		session.update(employee);		

		tx1.commit();
		session.close();
		return true;
	}

	@Override //delete employee tested working
	public boolean deleteEmployee(Employee employee) {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting employee, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting employee, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

	@Override //tested working
	public Employee getEmployeeById(long id) {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Employee employee;
		try {
			employee= (Employee) session.get(Employee.class, id);
			System.out.println("THE EMPLOYEE DETAILS ARE:" + employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employee details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employee details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return employee;
	}

	@Override  //tested working
	public List<Employee> getEmployeeByName(String name) {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<Employee> list= new ArrayList<Employee>();
		String sql = "SELECT * FROM Employee WHERE FIRST_NAME LIKE '"+name+"'	 ORDER BY FIRST_NAME ASC";		
		try {
			Query query = session.createSQLQuery(sql).addEntity(Employee.class);
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employee details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employee details, Rollback transaction done");
				e.printStackTrace();
				return list;
		} finally {
			session.close();
		}
		return list;
	}

	@Override  //tested working
	public List<Employee> getEmployeeByEmailId(String emailId) {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<Employee> list= new ArrayList<Employee>();
		String sql = "SELECT * FROM Employee WHERE OFFICE_EMAIL_ID LIKE '"+emailId+"'";		
		try {
			Query query = session.createSQLQuery(sql).addEntity(Employee.class);
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employee details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employee details, Rollback transaction done");
				e.printStackTrace();
				return list;
		} finally {
			session.close();
		}
		return list;

	}


	@Override
	public Employee getLogin(String userName, String password) {
		// TODO Auto-generated method stub
		Employee emp=null;
		List<Employee> results=null;
		long id=0l;
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria cr = session.createCriteria(Employee.class);
		Criterion empId=null;
		//emailid
		Criterion emailId=null;
		if(!userName.contains("@")){
			
			id=new Long(userName).longValue();
			//empId
			 empId = Restrictions.eq("empId", id);
			 cr.add(empId);
		}
		else
		{
			 emailId = Restrictions.eq("officeEmailId",userName);
			 cr.add(emailId);
		}
	
		
		
		
		
		//cr.add(Restrictions.or(empId, emailId));
		cr.add(Restrictions.eq("password", password));
		
		results=(List<Employee>)cr.list();
		
		if(null!=results && results.size()>0)
		{
			emp= results.get(0);
			
		}
		
		return emp;
	}


	@Override
	public String getEmployeePassword(long empId) throws HibernateException {
		// TODO Auto-generated method stub

		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		// use a Session to create the Criteria and we add Restrictions to
		// filter our results.
		Criteria crit = session.createCriteria(Employee.class);
		crit.add(Restrictions.eq("empId", empId));
		crit.setProjection(Projections.property("password"));
		System.out.println("EmpId:" + empId);
		// query for above code is "select password from Employee where empid=?"
		String password = (String) crit.list().get(0);
		System.out.println("EmpPassword:" + password);
		tx.commit();
		session.close();
		return password;

	}


	@Override  //tested working
	public List<Employee> getEmployeeList() throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<Employee> employee = session.createQuery("FROM Employee").list();
		return employee;
	}


}
