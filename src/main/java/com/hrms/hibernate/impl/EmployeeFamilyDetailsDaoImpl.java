package com.hrms.hibernate.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.EmployeeFamilyDetailsDao;
import com.hrms.entity.EmployeeFamilyDetails;

public class EmployeeFamilyDetailsDaoImpl implements  EmployeeFamilyDetailsDao{
	
	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();

	@Override
	public List<EmployeeFamilyDetails> getEmployeeFamilyDetails()
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<EmployeeFamilyDetails> employeefamilydetails = session.createQuery("FROM EmployeeFamilyDetails").list();
		return employeefamilydetails;
	}

	@Override
	public boolean saveEmployeeFamilyDetails(
			EmployeeFamilyDetails employeefamilydetails) throws HibernateException {
		// TODO Auto-generated method stub
		System.out.println("In save employeefamilydetails");
		System.out.println(employeefamilydetails);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(employeefamilydetails);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	@Override
	public boolean updateEmployeeFamilyDetails(
			EmployeeFamilyDetails employeefamilydetails) throws HibernateException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean deleteEmployeeFamilyDetails(
			EmployeeFamilyDetails employeefamilydetails) throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(employeefamilydetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting employeefamilydetails, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting employeefamilydetails, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

	@Override
	public List<EmployeeFamilyDetails> getEmployeeFamilyDetailsByEmailId(
			String emailid) throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<EmployeeFamilyDetails> list= new ArrayList<EmployeeFamilyDetails>();
		String sql = "SELECT * FROM Employee WHERE EMAIL_ID LIKE '"+emailid+"'	 ORDER BY FIRST_NAME ASC";		
		try {
			Query query = session.createSQLQuery(sql).addEntity(EmployeeFamilyDetails.class);
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employeefamily details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employeefamily details, Rollback transaction done");
				e.printStackTrace();
				return list;
		} finally {
			session.close();
		}
		return list;
	}

	@Override
	public EmployeeFamilyDetails getEmployeeFamilyDetailsByEmpId(Long empId)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		EmployeeFamilyDetails employeefamilydetails;
		try {
			employeefamilydetails= (EmployeeFamilyDetails) session.get(EmployeeFamilyDetails.class, empId);
			System.out.println("THE EMPLOYEE DETAILS ARE:" + employeefamilydetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employeefamily details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employeefamily details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return employeefamilydetails;
	}

}
