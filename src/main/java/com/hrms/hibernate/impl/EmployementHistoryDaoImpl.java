package com.hrms.hibernate.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;




import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hrms.dao.hibernate.EmployementHistoryDao;
import com.hrms.entity.Employee;
import com.hrms.entity.EmployeeProject;
import com.hrms.entity.EmployementHistory;


public class EmployementHistoryDaoImpl implements EmployementHistoryDao {
	
	@Autowired
	private SessionFactory factory = new Configuration().configure()
			.buildSessionFactory();

	HibernateTemplate template;

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}


	@Override
	public List<EmployementHistory> getEmployementHistory(long empId)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<EmployementHistory> list= new ArrayList<EmployementHistory>();
		String sql = "SELECT * FROM EmployementHistory WHERE ID LIKE '"+empId+"'";		
		try {
			Query query = session.createSQLQuery(sql).addEntity(EmployementHistory.class);
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching EmployementHistory details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching EmployementHistory details, Rollback transaction done");
				e.printStackTrace();
				return list;
		} finally {
			session.close();
		}
		return list;
	}

	@Override
	public boolean saveEmployementHistory(EmployementHistory employementhistory)
			throws HibernateException {
		System.out.println("In save employementhistory");
		System.out.println(employementhistory);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(employementhistory);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
		
	}

	@Override
	public boolean updateEmployementHistory(
			EmployementHistory employementhistory) throws HibernateException {
		//long empId = employee.getEmpId();
		Session session = factory.openSession();
		Transaction tx1 = session.beginTransaction();
		session.update(employementhistory);		
		tx1.commit();
		session.close();
		return true;
	}

	@Override
	public boolean deleteEmployementHistory(EmployementHistory employementhistory)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(employementhistory);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting employementhistory, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting employementhistory, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
		
	}

	
	
}
