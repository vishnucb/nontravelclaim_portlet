package com.hrms.hibernate.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hrms.dao.hibernate.SkillDetailsDao;
import com.hrms.entity.Employee;
import com.hrms.entity.SkillDetails;

public class SkillDetailsDaoImpl implements SkillDetailsDao {	
	
	@Autowired
	private SessionFactory factory = new Configuration().configure()
			.buildSessionFactory();

	HibernateTemplate template;

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}

	@Override
	public List<SkillDetails> getAllSkillDetails() throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean saveSkillDetails(SkillDetails employeeproject)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateSkillDetail(SkillDetails EmployeeProject)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteSkillDetail(SkillDetails EmployeeProject)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public SkillDetails getSkillBySkillId(long skillId)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		SkillDetails skillDetails;
		try {
			skillDetails= (SkillDetails) session.get(Employee.class, skillId);
			System.out.println("THE EMPLOYEE DETAILS ARE:" + skillDetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching skillDetails details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching skillDetails details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return skillDetails;
	}

	@Override
	public List<SkillDetails> getSkillByEmpId(long empId) {
		// TODO Auto-generated method stub
		List list = null;
		
		return list;
	}

	
	
}
