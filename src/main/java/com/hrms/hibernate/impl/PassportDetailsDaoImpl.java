package com.hrms.hibernate.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hrms.dao.hibernate.PassportDetailsDao;
import com.hrms.entity.Employee;
import com.hrms.entity.PassportDetails;

public class PassportDetailsDaoImpl implements PassportDetailsDao {

	@Autowired
	private SessionFactory factory = new Configuration().configure()
			.buildSessionFactory();

	HibernateTemplate template;

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}

	@Override
	public List<PassportDetails> getPassportDetails() throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<PassportDetails> passportList = session.createQuery("FROM PassportDetails").list();
		return passportList;
	}

	@Override
	public boolean savePassportDetails(PassportDetails passportDetails)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(passportDetails);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	@Override
	public boolean updatePassportDetails(PassportDetails passportDetails)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx1 = session.beginTransaction();
		session.update(passportDetails);		
		tx1.commit();
		session.close();
		return true;
	}

	@Override
	public boolean deletePassportDetails(PassportDetails passportDetails)
			throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(passportDetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting passportDetails, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting passportDetails, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

	@Override
	public PassportDetails getPassportByPassportNumber(String id)throws HibernateException {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		PassportDetails passportDetails;
		try {
			passportDetails= (PassportDetails) session.get(Employee.class, id);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching passportDetails details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching passportDetails details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return passportDetails;
	}

	@Override
	public PassportDetails getPassportByEmpId(long empId) {
		// TODO Auto-generated method stub
		return new PassportDetails();
	}

	
	

}
