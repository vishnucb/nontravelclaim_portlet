package com.hrms.hibernate.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.EducationDetailsDao;
import com.hrms.entity.Address;
import com.hrms.entity.EducationDetails;
import com.hrms.entity.Employee;
import com.hrms.entity.SkillDetails;

public class EducationDetailsDaoImpl implements EducationDetailsDao {

	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();
	
	
	@Override
	public List<EducationDetails> getEducationDetail()
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<EducationDetails> edudetails = session.createQuery("FROM EducationDetails").list();
		return edudetails;
	}

	

	@Override
	public boolean updateEducationDetails(EducationDetails educationdetails, long eduId)
			throws HibernateException { // Added by Vishnu
		
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		
		
		try{
		
		
		Query query=session.createQuery("update EducationDetails set collegeName=:collegeName, universityName=:universityName, joinDate=:joinDate, endDate=:endDate where id=:id and empId=:empId ");
		
		query.setString("collegeName", educationdetails.getCollegeName());
		query.setString("universityName", educationdetails.getUniversityName());
		
		query.setLong("id", eduId);
		query.setLong("empId", educationdetails.getEmpId().getEmpId());
		
	

	      query.setDate("joinDate", educationdetails.getJoinDate());
			query.setDate("endDate", educationdetails.getEndDate());
	      

		
		int modifications=query.executeUpdate();
		tx.commit();
		return true;
		
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting educationdetails, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting educationdetails, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	@Override
	public boolean saveEducationDetails(EducationDetails educationdetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		System.out.println("In save EducationDetails");
		System.out.println(educationdetails);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(educationdetails);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}



	@Override
	public boolean deleteEducationDetails(EducationDetails educationdetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(educationdetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting educationdetails, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting educationdetails, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}



	@Override
	public EducationDetails getEducationDetailsByEduId(Long eduId)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		EducationDetails edudetails;
		try {
			edudetails= (EducationDetails) session.get(EducationDetails.class, eduId);
			System.out.println("THE EDUCATION DETAILS ARE:" + edudetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching education details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching education details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return edudetails;
	}



	@Override
	public EducationDetails getEducationDetailsByEmpId(Long empId)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		EducationDetails edudetails;
		try {
			edudetails= (EducationDetails) session.get(EducationDetails.class, empId);
			System.out.println("THE EDUCATION DETAILS ARE:" + edudetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching education details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching education details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return edudetails;
	}
	
	

}
