package com.hrms.hibernate.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.CountryDao;
import com.hrms.entity.Country;

public class CountryDaoImpl implements CountryDao{
	
	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();

	@Override
	public List<Country> getCountry() throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<Country> country = session.createQuery("FROM Country").list();
		return country;
	}

	@Override
	public boolean saveCountry(Country country) throws HibernateException {
		// TODO Auto-generated method stub
		System.out.println("In save employee");
		System.out.println(country);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(country);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	@Override
	public boolean updateCountry(Country country) throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteCountry(Country country) throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(country);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting country, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting country, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

	@Override
	public List<Country> getCountryByCountryName(String countryname)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<Country> list= new ArrayList<Country>();
		String sql = "SELECT * FROM Country WHERE COUNTRY_NAME LIKE '"+countryname+"'";		
		try {
			Query query = session.createSQLQuery(sql).addEntity(Country.class);
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching country details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching country details, Rollback transaction done");
				e.printStackTrace();
				return list;
		} finally {
			session.close();
		}
		return list;
	}

}
