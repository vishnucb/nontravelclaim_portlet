package com.hrms.hibernate.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.NonTravelClaimDao;
import com.hrms.entity.NonTravelClaim;

public class NonTravelClaimDaoImpl implements NonTravelClaimDao{

	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();
	
	
	@Override
	public boolean saveNonTravelClaim(NonTravelClaim nontravelclaim)
			throws HibernateException {
		System.out.println("In save NonTravelClaim");
		System.out.println(nontravelclaim);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(nontravelclaim);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	@Override
	public boolean updateNonTravelClaim(NonTravelClaim nontravelclaim)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public NonTravelClaim getNonTravelClaim(NonTravelClaim nontravelclaim)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

}
