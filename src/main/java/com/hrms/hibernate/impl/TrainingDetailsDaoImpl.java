package com.hrms.hibernate.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.hibernate.TrainingDetailsDao;
import com.hrms.entity.TrainingDetails;

public class TrainingDetailsDaoImpl implements TrainingDetailsDao{
	
	
	@Autowired
	private SessionFactory factory=new Configuration().configure().buildSessionFactory();

	@Override
	public List<TrainingDetails> getTrainingDetails() throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		List<TrainingDetails> trainingdetails = session.createQuery("FROM TrainingDetails").list();
		return trainingdetails;
	}

	@Override
	public boolean saveTrainingDetails(TrainingDetails trainingdetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		System.out.println("In save trainingdetails");
		System.out.println(trainingdetails);
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(trainingdetails);
			System.out.println("save method before comit");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while saving transaction, Rollback in process....");
				tx.rollback();
			System.out.println("Error while saving transaction, Rollback transaction done");
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	@Override
	public boolean updateTrainingDetails(TrainingDetails trainingdetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteTrainingDetails(TrainingDetails trainingdetails)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();		
		session.close();
		try {
			session.delete(trainingdetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while deleting trainingdetails, Rollback in process....");
				tx.rollback();
				System.out.println("Error while deleting trainingdetails, Rollback transaction done");
				e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		
		return true;
	}

	@Override
	public TrainingDetails getTrainingDetailsByTrainingId(Long id)
			throws HibernateException {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		TrainingDetails trainingdetails;
		try {
			trainingdetails= (TrainingDetails) session.get(TrainingDetails.class, id);
			System.out.println("THE EMPLOYEE DETAILS ARE:" + trainingdetails);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employee details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employee details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return trainingdetails;
	}

}
