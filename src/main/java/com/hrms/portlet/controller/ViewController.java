package com.hrms.portlet.controller;

import com.hrms.model.Customer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.mimacom.sample.spring.portlet.controller.Keys.Views;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by alberto.martinez@mimacom.com on 2/06/14.
 */

@Controller
@RequestMapping("VIEW")
public class ViewController {
   
	private static final Log LOGGER = LogFactoryUtil.getLog(ViewController.class);

    @RenderMapping
    public String defaultView() {

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Default View");
        }

        return Views.DEFAULT_VIEW;
    }

    @RenderMapping(params = "render=alternative-view")
    public String alternativeView() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Alternative view");
        }

        return Views.ALTERNATIVE_VIEW;
    }

    @ActionMapping(params = "action=action-one")
    public void actionOne() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Action one");
        }

        // Returns control to default view
    }

    @ActionMapping(params = "action=action-two")
    public void actionTwo(ActionResponse actionResponse) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Action two");
        }

        // Returns control to alternative view
        actionResponse.setRenderParameter("render", "alternative-view");
    }

    @ResourceMapping(value = "resource-one")
    public void resourceOne(ResourceResponse resourceResponse) throws PortletException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Resource one");
        }

        try {
            resourceResponse.setContentType("text/html");
            PrintWriter writer = resourceResponse.getWriter();
            writer.println("<p>This request handle the complete response. This is usefull to return JSON, images, files or any other resource that are needed by our portlets</p>");
            
        } catch (IOException e) {
            throw new PortletException(e);
        }
    }
    
   /* //@ActionMapping(params = "action=personalInfo")
   // @RenderMapping(params = "render=personalInfo")
    @RenderMapping(params = "action=personalInfo")
    //@RequestMapping(value = "/personalInfo", method=RequestMethod.GET)
    public String  personalInfo(Model model) throws PortletException  {
    	System.out.println("asasasasas");
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("personalInfo");
        }

        // Returns control to alternative view8
       // actionResponse.setRenderParameter("action", "viewPersonalInfo");
       return "viewPersonalInfo";
    }
    */
    @ActionMapping(params = "action=personalInfo-one")
    public void testRenderMethod(ActionResponse actionResponse){
    	if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("testRenderMethod");
        }
    	
    	actionResponse.setRenderParameter("render", "personalInfo-view");
    	
    }
    
    
    @RenderMapping(params = "render=personalInfo-view")
    public String personalInfoView(Map<String, Object> map) {
    	
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("personalInfoView");
        }
        
        Customer customer = new Customer();
        map.put("customer", customer);
        return "form";
    }
    
    //@ActionMapping(params = "action=handleCustomer")
	@ActionMapping(value = "handleCustomer")
	public void getCustomerData(
			@ModelAttribute("customer") Customer customer,
			ActionRequest actionRequest, ActionResponse actionResponse,
			Model model) {
    	
    	System.out.println(customer.getFirstName());
    	actionResponse.setRenderParameter("action", "success");

		model.addAttribute("successModel", customer);
    	//actionResponse.setRenderParameter("render", "personalInfo");
    }
	@RenderMapping(params = "action=success")
	public String viewSuccess() {

		LOGGER.info("#############Calling viewSuccess###########");
		/**
		 * Display success.jsp
		 */
		return "success";

	}
}
