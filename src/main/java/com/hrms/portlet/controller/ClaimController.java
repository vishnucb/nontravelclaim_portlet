/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  com.hrms.model.NonTravelClaim
 *  com.liferay.portal.kernel.log.Log
 *  com.liferay.portal.kernel.log.LogFactoryUtil
 *  javax.portlet.ActionRequest
 *  javax.portlet.ActionResponse
 *  javax.portlet.RenderRequest
 *  javax.portlet.RenderResponse
 *  org.springframework.stereotype.Controller
 *  org.springframework.ui.Model
 *  org.springframework.validation.BindingResult
 *  org.springframework.web.bind.annotation.ModelAttribute
 *  org.springframework.web.bind.annotation.RequestMapping
 *  org.springframework.web.portlet.bind.annotation.ActionMapping
 *  org.springframework.web.portlet.bind.annotation.RenderMapping
 */
package com.hrms.portlet.controller;

import com.hrms.model.NonTravelClaim;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import java.util.ArrayList;
import java.util.Map;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller(value="helloWorldController")
@RequestMapping(value={"VIEW"})
public class ClaimController {
    private Log log = LogFactoryUtil.getLog((String)ClaimController.class.getName());

    @RenderMapping
    public String viewHomePage(RenderRequest request, RenderResponse response) {
        this.log.info((Object)"#################Calling viewHomePage############");
        return "view";
    }

    @RenderMapping(params={"action=showForm"})
    public String viewByParameter(Map<String, Object> map) {
        this.log.info((Object)"##############Calling viewByParameter###########");
        NonTravelClaim ntc = new NonTravelClaim();
        map.put("nontravelclaim", (Object)ntc);
        ArrayList<String> typeOfClaimList = new ArrayList<String>();
        typeOfClaimList.add("Certification");
        typeOfClaimList.add("Food reimbursment");
        typeOfClaimList.add("Cab");
        typeOfClaimList.add("Other");
        map.put("typeOfClaimList", typeOfClaimList);
        return "form";
    }

    @RenderMapping(params={"action=Certification"})
    public String viewSuccess1() {
        this.log.info((Object)"#############Calling viewSuccess###########");
        return "expensesClaimCertification";
    }

    @RenderMapping(params={"action=Food reimbursment"})
    public String viewSuccess2() {
        this.log.info((Object)"#############Calling viewSuccess###########");
        return "expensesClaimFood";
    }

    @RenderMapping(params={"action=Cab"})
    public String viewSuccess3() {
        this.log.info((Object)"#############Calling viewSuccess###########");
        return "expensesClaimCab";
    }

    @RenderMapping(params={"action=Other"})
    public String viewSuccess4() {
        this.log.info((Object)"#############Calling viewSuccess###########");
        return "expensesClaimOther";
    }

    @ActionMapping(value="handleCustomer")
    public void getCustomerData(@ModelAttribute(value="nontravelclaim") NonTravelClaim nontravelclaim, BindingResult result, ActionRequest actionRequest, ActionResponse actionResponse, Model model) {
        actionResponse.setRenderParameter("action", nontravelclaim.getTypeOfClaim());
        model.addAttribute("successModel", (Object)nontravelclaim);
        if (nontravelclaim.getTypeOfClaim().equals("Certification")) {
            ArrayList<String> certificationTypeList = new ArrayList<String>();
            ArrayList<String> certificationNameList = new ArrayList<String>();
            certificationTypeList.add("Java");
            certificationTypeList.add("Loma");
            certificationTypeList.add("ABAP");
            model.addAttribute("certificationTypeList", certificationTypeList);
            certificationNameList.add("LOMA 280");
            certificationNameList.add("Java 8");
            certificationNameList.add("ABAP");
            model.addAttribute("certificationNameList", certificationNameList);
        }
    }

    @ActionMapping(value="handleSaveData")
    public void saveNonTravelClaimData(@ModelAttribute(value="nontravelclaim") NonTravelClaim nontravelclaim, BindingResult result, ActionRequest actionRequest, ActionResponse actionResponse, Model model) {
    }
}
