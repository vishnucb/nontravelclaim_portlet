package com.hrms.entity;
import java.sql.Blob;
import java.util.Date;

public class VisaDetails  {

	private String visaNumber;
	private String countryId;
	private VisaType visaType;
	private Date fromDate;
	private Date toDate;
	private String restrictions;
	private Blob uploadFile;
	private Employee employeeId;
	
	public String getVisaNumber() {
		return visaNumber;
	}
	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public VisaType getVisaType() {
		return visaType;
	}
	public void setVisaType(VisaType visaType) {
		this.visaType = visaType;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getRestrictions() {
		return restrictions;
	}
	public void setRestrictions(String restrictions) {
		this.restrictions = restrictions;
	}
	public Blob getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(Blob uploadFile) {
		this.uploadFile = uploadFile;
	}
	public Employee getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Employee employeeId) {
		this.employeeId = employeeId;
	}
	
	
	
}
