package com.hrms.entity;

import java.sql.Blob;
import java.util.Date;



public class TrainingDetails {
	
	private long trainingId;  //generate sequence
	private String name;
	private Date startDate;
	private Date endDate;
	private long proficiency;
	private String source;
	private Blob scanCopy;
	
	
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public long getProficiency() {
		return proficiency;
	}
	public void setProficiency(long proficiency) {
		this.proficiency = proficiency;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Blob getScanCopy() {
		return scanCopy;
	}
	public void setScanCopy(Blob scanCopy) {
		this.scanCopy = scanCopy;
	}
	public long getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(long trainingId) {
		this.trainingId = trainingId;
	}

	
	
	
}
