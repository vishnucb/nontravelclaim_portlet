package com.hrms.entity;

import java.util.Date;
import java.util.Set;

public class EmployeeFamilyDetails {

	
	private long id;
	private Employee empId;
	private String fName;
	private String mName;
	private String lName;
	private String relation;
	private String gender;
	private Date  dateOfBirth;
	private String emailId;
	private long mobileNo;
	private String occupation;
	private PassportDetails passportId;
	private Address addressId;
	private Set<VisaDetails> visaId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public long getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
	public Set<VisaDetails> getVisaId() {
		return visaId;
	}
	public void setVisaId(Set<VisaDetails> visaId) {
		this.visaId = visaId;
	}
	public Employee getEmpId() {
		return empId;
	}
	public void setEmpId(Employee empId) {
		this.empId = empId;
	}
	public PassportDetails getPassportId() {
		return passportId;
	}
	public void setPassportId(PassportDetails passportId) {
		this.passportId = passportId;
	}
	public Address getAddressId() {
		return addressId;
	}
	public void setAddressId(Address addressId) {
		this.addressId = addressId;
	}
	
	
	
}
