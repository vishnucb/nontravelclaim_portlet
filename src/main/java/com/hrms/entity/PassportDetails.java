package com.hrms.entity;

import java.sql.Blob;
import java.util.Date;
import java.util.Set;




public class PassportDetails {
	
	private Employee employeeId;
	private String passportNo;
	private Date issueDate;
	private Date expiryDate;
	private String issueCountryName;
	private String issuePlace;
	private Blob scanCopy;
	private String fileLocation;
	private String fileName;
	private Set<EmployeeFamilyDetails> employeeFamilyDetails;
	
	
	public Employee getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Employee employeeId) {
		this.employeeId = employeeId;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getIssueCountryName() {
		return issueCountryName;
	}
	public void setIssueCountryName(String issueCountryName) {
		this.issueCountryName = issueCountryName;
	}
	public String getIssuePlace() {
		return issuePlace;
	}
	public void setIssuePlace(String issuePlace) {
		this.issuePlace = issuePlace;
	}
	public Blob getScanCopy() {
		return scanCopy;
	}
	public void setScanCopy(Blob scanCopy) {
		this.scanCopy = scanCopy;
	}
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Set<EmployeeFamilyDetails> getEmployeeFamilyDetails() {
		return employeeFamilyDetails;
	}
	public void setEmployeeFamilyDetails(
			Set<EmployeeFamilyDetails> employeeFamilyDetails) {
		this.employeeFamilyDetails = employeeFamilyDetails;
	}
	
	
}
