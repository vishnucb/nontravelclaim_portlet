package com.hrms.entity;

import java.sql.Blob;

import java.util.Date;
public class CertificationDetails {

	private Employee empId;  //from employee entity
	private long certId;  //generate sequence
	private String certNumber;
	private String certName;
	private String version;
	private float percentage;
	private Date startDate;
	private Date endDate;
	private Blob uploadFile;
	
	public Employee getEmpId() {
		return empId;
	}
	public void setEmpId(Employee empId) {
		this.empId = empId;
	}
	public long getCertId() {
		return certId;
	}
	public void setCertId(long certId) {
		this.certId = certId;
	}
	public String getCertNumber() {
		return certNumber;
	}
	public void setCertNumber(String certNumber) {
		this.certNumber = certNumber;
	}
	public String getCertName() {
		return certName;
	}
	public void setCertName(String certName) {
		this.certName = certName;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Blob getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(Blob uploadFile) {
		this.uploadFile = uploadFile;
	}
	
	
	
	
	
	
}
