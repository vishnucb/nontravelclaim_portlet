package com.hrms.entity;

import java.sql.Blob;
import java.util.Date;

public class EducationDetails {
	
	private Employee empId;// from employee entity
	private long eduId; //auto generate sequence
	private QualificationType qualificationId; // from QualificationType
	private DegreeType degreeId;
	private Specialization specId;
	private String universityName;
	private String collegeName;
	private Date joinDate;
	private Date endDate;
	private float percentage;
	private Blob uploadDocs;
	
	public Employee getEmpId() {
		return empId;
	}
	public void setEmpId(Employee empId) {
		this.empId = empId;
	}
	public long getEduId() {
		return eduId;
	}
	public void setEduId(long eduId) {
		this.eduId = eduId;
	}
	public QualificationType getQualificationId() {
		return qualificationId;
	}
	public void setQualificationId(QualificationType qualificationId) {
		this.qualificationId = qualificationId;
	}
	public DegreeType getDegreeId() {
		return degreeId;
	}
	public void setDegreeId(DegreeType degreeId) {
		this.degreeId = degreeId;
	}
	public Specialization getSpecId() {
		return specId;
	}
	public void setSpecId(Specialization specId) {
		this.specId = specId;
	}
	public String getUniversityName() {
		return universityName;
	}
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	public Blob getUploadDocs() {
		return uploadDocs;
	}
	public void setUploadDocs(Blob uploadDocs) {
		this.uploadDocs = uploadDocs;
	}
	
	
	
	
	
	

}
