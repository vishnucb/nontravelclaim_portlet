package com.hrms.entity;

public class TechnologyType {
	
	private long technologyId; //auto generate sequence
	private String name;
	
	
	
	public long getTechnologyId() {
		return technologyId;
	}
	public void setTechnologyId(long technologyId) {
		this.technologyId = technologyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
