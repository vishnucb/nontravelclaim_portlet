package com.hrms.entity;

import java.util.Date;
import java.util.Set;

public class NonTravelClaim {

	private int transactionID;
	private String typeOfClaim;
	private Date dateForClaim;
	private double claimAmount;
	private String certificationType;
	private String certificationName;
	private String description;
	
	private Employee employee;
	
	
	public String getTypeOfClaim() {
		return typeOfClaim;
	}
	public Date getDateForClaim() {
		return dateForClaim;
	}
	public double getClaimAmount() {
		return claimAmount;
	}
	public String getCertificationType() {
		return certificationType;
	}
	public String getCertificationName() {
		return certificationName;
	}
	public String getDescription() {
		return description;
	}
	public void setTypeOfClaim(String typeOfClaim) {
		this.typeOfClaim = typeOfClaim;
	}
	public void setDateForClaim(Date dateForClaim) {
		this.dateForClaim = dateForClaim;
	}
	public void setClaimAmount(double claimAmount) {
		this.claimAmount = claimAmount;
	}
	public void setCertificationType(String certificationType) {
		this.certificationType = certificationType;
	}
	public void setCertificationName(String certificationName) {
		this.certificationName = certificationName;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((certificationName == null) ? 0 : certificationName.hashCode());
		result = prime * result + ((certificationType == null) ? 0 : certificationType.hashCode());
		long temp;
		temp = Double.doubleToLongBits(claimAmount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((dateForClaim == null) ? 0 : dateForClaim.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((typeOfClaim == null) ? 0 : typeOfClaim.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NonTravelClaim other = (NonTravelClaim) obj;
		if (certificationName == null) {
			if (other.certificationName != null)
				return false;
		} else if (!certificationName.equals(other.certificationName))
			return false;
		if (certificationType == null) {
			if (other.certificationType != null)
				return false;
		} else if (!certificationType.equals(other.certificationType))
			return false;
		if (Double.doubleToLongBits(claimAmount) != Double.doubleToLongBits(other.claimAmount))
			return false;
		if (dateForClaim == null) {
			if (other.dateForClaim != null)
				return false;
		} else if (!dateForClaim.equals(other.dateForClaim))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (typeOfClaim == null) {
			if (other.typeOfClaim != null)
				return false;
		} else if (!typeOfClaim.equals(other.typeOfClaim))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "NonTravelClaim [typeOfClaim=" + typeOfClaim + ", dateForClaim=" + dateForClaim + ", claimAmount="
				+ claimAmount + ", certificationType=" + certificationType + ", certificationName=" + certificationName
				+ ", description=" + description + "]";
	}
	public int getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
}
