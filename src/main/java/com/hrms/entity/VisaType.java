package com.hrms.entity;

public class VisaType {
	
	private long visaId;  //sequence generate
	private String name;
	private Country countryId;
	
	
	public long getVisaId() {
		return visaId;
	}
	public void setVisaId(long visaId) {
		this.visaId = visaId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Country getCountryId() {
		return countryId;
	}
	public void setCountryId(Country countryId) {
		this.countryId = countryId;
	}
	
	
	
	

}
