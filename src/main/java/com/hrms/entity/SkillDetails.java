package com.hrms.entity;
import java.util.Date;

public class SkillDetails {
	
	private long skillId; //generate sequence
	private Employee empId;   //from Employee entity
	private TechnologyType technologyId;  //from TechnologyType entity
	private String version;
	private long proficiency;
	private Date lastUsed;
	private double relevantExp;
	//private TrainingDetails trainingId;   //from TrainingDetails entity
	
	
	public long getSkillId() {
		return skillId;
	}
	public void setSkillId(long skillId) {
		this.skillId = skillId;
	}
	public Employee getEmpId() {
		return empId;
	}
	public void setEmpId(Employee empId) {
		this.empId = empId;
	}
	public TechnologyType getTechnologyId() {
		return technologyId;
	}
	public void setTechnologyId(TechnologyType technologyId) {
		this.technologyId = technologyId;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public long getProficiency() {
		return proficiency;
	}
	public void setProficiency(long proficiency) {
		this.proficiency = proficiency;
	}
	public Date getLastUsed() {
		return lastUsed;
	}
	public void setLastUsed(Date lastUsed) {
		this.lastUsed = lastUsed;
	}
	public double getRelevantExp() {
		return relevantExp;
	}
	public void setRelevantExp(double relevantExp) {
		this.relevantExp = relevantExp;
	}
	/*public TrainingDetails getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(TrainingDetails trainingId) {
		this.trainingId = trainingId;
	}*/
	
	
	
	
	
	
	
}
