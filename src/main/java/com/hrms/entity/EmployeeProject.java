package com.hrms.entity;

public class EmployeeProject {
	

	private long projId;	//generate sequence
	private Employee empId; //from Employee entity
	private String name;   //project name
	private String desc;  //project description
	private String role;  //employee role

	
	
	public long getProjId() {
		return projId;
	}
	public void setProjId(long projId) {
		this.projId = projId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Employee getEmpId() {
		return empId;
	}
	public void setEmpId(Employee empId) {
		this.empId = empId;
	}  
	
	
}
