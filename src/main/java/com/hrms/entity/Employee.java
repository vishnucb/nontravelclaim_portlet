/**
 * 
 */
package com.hrms.entity;

import java.util.Date;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class Employee{
	
	private long empId;
	private String fName;
	private String mName;
	private String lName;
	private String password;
	private String gender;
	private Date  dateOfBirth;
	private Date dateOfJoining;
	private String designation;
	private String officeEmailId;
	private String personalEmailId;
	private String alternateEmailId;
	private String totalExperience;
	private long mobileNo;
	private long officeNo;
	private long homeNo;
	private long emergencyNo;
	private String panNo;
	private String uanNo;
	private String pfNo;
	private long noticePeriod;
	private boolean admin;
	private Set<PassportDetails> passportId;
	/*private Set<Address> addressId;*/
	private Set<VisaDetails> visaId;
	private Set<EducationDetails> educationDetails;
	
	
	
	
	public Employee() {
		
	}
	
	public Employee(long empId, String fName, String mName, String lName,
			String password, String gender, Date dateOfBirth,
			Date dateOfJoining, String designation, String officeEmailId,
			String personalEmailId, String alternateEmailId,
			String totalExperience, long mobileNo, long officeNo, long homeNo,
			long emergencyNo, String panNo, String uanNo, String pfNo,
			long noticePeriod, Set<PassportDetails> passportId,
			Set<Address> addressId, Set<VisaDetails> visaId,
			Set<EducationDetails> educationDetails) {
		super();
		this.empId = empId;
		this.fName = fName;
		this.mName = mName;
		this.lName = lName;
		this.password = password;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.dateOfJoining = dateOfJoining;
		this.designation = designation;
		this.officeEmailId = officeEmailId;
		this.personalEmailId = personalEmailId;
		this.alternateEmailId = alternateEmailId;
		this.totalExperience = totalExperience;
		this.mobileNo = mobileNo;
		this.officeNo = officeNo;
		this.homeNo = homeNo;
		this.emergencyNo = emergencyNo;
		this.panNo = panNo;
		this.uanNo = uanNo;
		this.pfNo = pfNo;
		this.noticePeriod = noticePeriod;
		this.passportId = passportId;
		//this.addressId = addressId;
		this.visaId = visaId;
		this.educationDetails = educationDetails;
	}

	public long getEmpId() {
		return empId;
	}
	public void setEmpId(long empId) {
		this.empId = empId;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Date getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getOfficeEmailId() {
		return officeEmailId;
	}
	public void setOfficeEmailId(String officeEmailId) {
		this.officeEmailId = officeEmailId;
	}
	public String getPersonalEmailId() {
		return personalEmailId;
	}
	public void setPersonalEmailId(String personalEmailId) {
		this.personalEmailId = personalEmailId;
	}
	public String getAlternateEmailId() {
		return alternateEmailId;
	}
	public void setAlternateEmailId(String alternateEmailId) {
		this.alternateEmailId = alternateEmailId;
	}
	public String getTotalExperience() {
		return totalExperience;
	}
	public void setTotalExperience(String totalExperience) {
		this.totalExperience = totalExperience;
	}
	public long getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}
	public long getOfficeNo() {
		return officeNo;
	}
	public void setOfficeNo(long officeNo) {
		this.officeNo = officeNo;
	}
	public long getHomeNo() {
		return homeNo;
	}
	public void setHomeNo(long homeNo) {
		this.homeNo = homeNo;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getUanNo() {
		return uanNo;
	}
	public void setUanNo(String uanNo) {
		this.uanNo = uanNo;
	}
	public String getPfNo() {
		return pfNo;
	}
	public void setPfNo(String pfNo) {
		this.pfNo = pfNo;
	}
	public long getNoticePeriod() {
		return noticePeriod;
	}
	public void setNoticePeriod(long noticePeriod) {
		this.noticePeriod = noticePeriod;
	}
	public Set<PassportDetails> getPassportId() {
		return passportId;
	}
	public void setPassportId(Set<PassportDetails> passportId) {
		this.passportId = passportId;
	}
	/*public Set<Address> getAddressId() {
		return addressId;
	}
	public void setAddressId(Set<Address> addressId) {
		this.addressId = addressId;
	}*/
	public Set<VisaDetails> getVisaId() {
		return visaId;
	}
	public void setVisaId(Set<VisaDetails> visaId) {
		this.visaId = visaId;
	}
	public long getEmergencyNo() {
		return emergencyNo;
	}
	public void setEmergencyNo(long emergencyNo) {
		this.emergencyNo = emergencyNo;
	}
	public Set<EducationDetails> getEducationDetails() {
		return educationDetails;
	}
	public void setEducationDetails(Set<EducationDetails> educationDetails) {
		this.educationDetails = educationDetails;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	
	
	
}
