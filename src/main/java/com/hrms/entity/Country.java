package com.hrms.entity;

import java.util.Set;

public class Country {

	private Long contryId;
	private String countryName;
	private String currency;
	private String countryDetails;
	private Set<VisaType> visaTypes;
	
	public Long getContryId() {
		return contryId;
	}
	public void setContryId(Long contryId) {
		this.contryId = contryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCountryDetails() {
		return countryDetails;
	}
	public void setCountryDetails(String countryDetails) {
		this.countryDetails = countryDetails;
	}
	public Set<VisaType> getVisaTypes() {
		return visaTypes;
	}
	public void setVisaTypes(Set<VisaType> visaTypes) {
		this.visaTypes = visaTypes;
	}
	
	
}
